libdevel-dprof-perl (20110802.00-5) unstable; urgency=medium

  * Team upload.
  * Fix autopkgtests.
    The smoke test needs more files, and use.t doesn't make any sense
    for this module.
  * Declare compliance with Debian Policy 4.6.1.
  * Set Rules-Requires-Root: no.
  * Drop unversioned perl from Depends.
  * Enable all hardening flags.

 -- gregor herrmann <gregoa@debian.org>  Thu, 16 Jun 2022 17:56:11 +0200

libdevel-dprof-perl (20110802.00-4) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * Change bugtracker URL(s) to HTTPS.
  * Simplify BTS URL.
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Repository, Repository-
    Browse.

  [ gregor herrmann ]
  * debian/control: update Build-Depends for cross builds.
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Set Testsuite header for perl package.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 13 Jun 2022 15:53:51 +0100

libdevel-dprof-perl (20110802.00-3) unstable; urgency=low

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Florian Schlichting ]
  * Email change: Florian Schlichting -> fsfs@debian.org
  * Add myself to debian/copyright
  * Switch dh compatibility to level 9 to enable passing of hardening flags
  * Declare compliance with Debian Policy 3.9.5
  * Drop README from shipped docs

 -- Florian Schlichting <fsfs@debian.org>  Sun, 13 Apr 2014 16:54:36 +0200

libdevel-dprof-perl (20110802.00-2) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Dominic Hargreaves ]
  * Apply patch from Petr Písař fixing test failures with Perl 5.16
    (Closes: #676271)

 -- Dominic Hargreaves <dom@earth.li>  Sun, 05 May 2013 23:41:29 +0100

libdevel-dprof-perl (20110802.00-1) unstable; urgency=low

  [ Florian Schlichting ]
  * Imported Upstream version 20110802.00
  * Bump debhelper compatibility to 8 (no changes).
  * Added myself to Uploaders.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

 -- Florian Schlichting <fschlich@zedat.fu-berlin.de>  Fri, 25 Nov 2011 22:54:30 +0000

libdevel-dprof-perl (20110228.00-1) unstable; urgency=low

  * Initial Release. (Closes: #627114)

 -- Dominic Hargreaves <dom@earth.li>  Wed, 18 May 2011 23:00:25 +0100
